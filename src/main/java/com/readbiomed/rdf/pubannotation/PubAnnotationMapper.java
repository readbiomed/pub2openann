package com.readbiomed.rdf.pubannotation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openrdf.model.Namespace;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;
import org.openrdf.repository.sail.SailRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFWriter;
import org.openrdf.rio.Rio;
import org.openrdf.sail.memory.MemoryStore;

import com.readbiomed.rdf.annotation.RDFAnnotationModel;

public class PubAnnotationMapper {

	public static Repository REPO;
	public static String NAMESPACE = "http://pubannotation.org/";
	public static String GENIA_NS = "http://bionlp.dbcls.jp/ontology/geniatask.owl#";
	public static String PUBANN_NS = "http://pubannotation.org/ontology/pubannotation.owl#";
	public static ValueFactory valueFactory;
	public static RepositoryConnection REPOCONN;
	public static RDFAnnotationModel annotationModel;
	
	public static void setupRepository() throws RepositoryException {
		REPO = new SailRepository(new MemoryStore());
		REPO.initialize();
		
		valueFactory  = REPO.getValueFactory();
		REPOCONN = REPO.getConnection();
	}
	
	public static void setupModel() {
		annotationModel = new RDFAnnotationModel(REPOCONN);
		// Add PubAnnotation-specific namespaces
		annotationModel.setNamespace("pubann", PUBANN_NS);
		annotationModel.setNamespace("genia", GENIA_NS);
		
	}
	
	
	public static void writeStatements(RepositoryResult<Namespace> namespaces, RepositoryResult<Statement> statements) {
		try { 
			RDFWriter writer = Rio.createWriter(RDFFormat.JSONLD, System.out); //RDFFormat.TRIG, System.out);
//			annotationModel.addAll(statements);
//			Rio.write(annotationModel.getModel(), System.out, RDFFormat.TURTLE); // RDFFormat.JSONLD); // RDFFormat.TURTLE);

//			  Map<String,URI> nsMap=new HashMap<String,String>();
//			    while (namespaces.hasNext()) {
//			      Namespace ns=namespaces.next();
//			      nsMap.put(ns.getPrefix(),);
//			    }
//			    for (  String prefix : nsMap.keySet() ) {
//			        writer.handleNamespace(prefix, nsMap.get(prefix));
//			      }		
			    
			writer.startRDF();

			while ( statements.hasNext() ) {
				writer.handleStatement(statements.next());
			}
			writer.endRDF();

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void processPubAnnotationFile(String pubAnnotationFileName) throws RepositoryException {
		try {
			// read the json file
			FileReader reader = new FileReader(pubAnnotationFileName);

			JSONParser jsonParser = new JSONParser();
			JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);

			// get the target PubAnnotation division URI
			// "target":"http://pubannotation.org/docs/sourcedb/PMC/sourceid/1134658/divs/1"
			String targetURIStr = (String) jsonObject.get("target");
			String annotationPrefix = "";
			if (targetURIStr != null) {
	
				Pattern pattern = Pattern.compile("http://([\\w.]+)/docs/sourcedb/(\\w+)/sourceid/(\\w+)/divs/(\\d+)");
				Matcher matcher = pattern.matcher(targetURIStr);
	
				while (matcher.find()) {
					String baseURI = matcher.group(1);
					String source = matcher.group(2);
					String docID = matcher.group(3);
					String divID = matcher.group(4);
	
					annotationPrefix += source + "-" + docID + "-" + divID + "-";
				}
			} else {
				targetURIStr = "http://doc";
				annotationPrefix = "Doc-";
			}
			 
			// get the PubAnnotation division text
			// String text = (String) jsonObject.get("text");

			// get the PubAnnotation project name the annotations are associated with
			//String project = (String) jsonObject.get("project");

			
			// get the array of denotations
			// Each denotation should have internal structure such as:
			// {"id":"T1","span":{"begin":15,"end":43},"obj":"Protein"}
			JSONArray denotations = (JSONArray) jsonObject.get("denotations");
			if ( denotations != null ) {
				// process each denotation in turn
				Iterator<JSONObject> denotationIterator = denotations.iterator();
				while (denotationIterator.hasNext()) {
					JSONObject denotation = (JSONObject) denotationIterator.next();
					String denotationID = (String)denotation.get("id");
					JSONObject span = (JSONObject) denotation.get("span");
					int spanStart = ((Long)span.get("begin")).intValue();
					int spanEnd = ((Long)span.get("end")).intValue();
					String denotationType = (String)denotation.get("obj");
					URI denotationURI = valueFactory.createURI(GENIA_NS, denotationType);
					annotationModel.addEntity(NAMESPACE, annotationPrefix, targetURIStr+".txt", denotationID, spanStart, spanEnd, denotationURI);
				}
			}

			// get the array of relations
			// Each relation should have internal structure such as:
			// {"id":"R10","pred":"themeOf","subj":"T13","obj":"E1"}
			JSONArray relations = (JSONArray) jsonObject.get("relations");
			if ( relations != null ) {
			// process each relation in turn
				Iterator<JSONObject> relationIterator = relations.iterator();
				while (relationIterator.hasNext()) {
					JSONObject relation = (JSONObject) relationIterator.next();
					Set<String> keys = (Set<String>)relation.keySet();
					String id = (String)relation.get("id");
					String pred = (String) relation.get("pred");
					URI predURI = valueFactory.createURI(GENIA_NS, pred);

					String subj = (String) relation.get("subj");
					String obj = (String) relation.get("obj");
					annotationModel.addRelation(NAMESPACE, annotationPrefix, targetURIStr+".txt", id, predURI, subj, obj);

				}		
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ParseException ex) {
			ex.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.out.println("Specify PubAnnotation json file to process: PubAnnotationMapper <path/to/filename>");
			return;
		}
		
		try {
			setupRepository();
			setupModel();
			processPubAnnotationFile(args[0]);
			// get all the statements in the repository and write them out
			writeStatements(REPOCONN.getNamespaces(), REPOCONN.getStatements(null, null, null, true));
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
