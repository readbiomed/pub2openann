package com.readbiomed.rdf.annotation;

import java.util.HashMap;

import info.aduna.iteration.Iterations;

import org.openrdf.model.Model;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.model.vocabulary.DCTERMS;
import org.openrdf.model.vocabulary.FOAF;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.model.vocabulary.XMLSchema;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.RepositoryResult;


public class RDFAnnotationModel {
	Model model;
	RepositoryConnection repositoryConn;
	ValueFactory valueFactory;
	int srCnt; // counter for SpecificResource object labels
	int annCnt; // counter for Annotation object labels
	HashMap<String,URI> idMap; // mapping table for PubAnnotation IDs to RDF IDs
	HashMap<String,URI> assertionMap; // mapping table for PubAnnotation IDs to their Assertion contexts
	
	public URI OA_ANNOTATION;
	public URI OA_HAS_TARGET;
	public URI OA_HAS_BODY;
	public URI OA_TEXT_POSITION_SELECTOR;
	public URI OA_SPECIFIC_RESOURCE;
	public URI OA_SERIALIZED_BY;
	public URI OA_MOTIVATED_BY;
	public URI OA_TAGGING;
	public URI OA_HAS_SOURCE;
	public URI OA_HAS_SELECTOR;
	public URI OA_START;
	public URI OA_END;
	public URI SIO_REFERS_TO;
	public URI SUBCLASS;
	public URI PUB_ANNOTATION;
	
	public static String DCTYPES_NS = "http://purl.org/dc/dcmitype/";
	public static String PROV_NS = "http://www.w3.org/ns/prov#";
	public static String OA_NS = "http://www.w3.org/ns/oa#";
	public static String SIO_NS = "http://semanticscience.org/resource/";
	public static String PAV_NS = "http://purl.org/pav/";
	public static String GENE_NS = "http://www.ncbi.nlm.nih.gov/gene/";
	public static String GO_NS = "http://purl.org/obo/owl/GO#";

	
	public RDFAnnotationModel(RepositoryConnection repoConnection) {
		this.model = new LinkedHashModel();
		this.idMap = new HashMap<String,URI>();
		this.assertionMap = new HashMap<String,URI>();
		this.repositoryConn = repoConnection;
		valueFactory = repositoryConn.getValueFactory();
		
		model.setNamespace("rdf", RDF.NAMESPACE);
		model.setNamespace("rdfs", RDFS.NAMESPACE);
		model.setNamespace("xsd", XMLSchema.NAMESPACE);
		model.setNamespace("foaf", FOAF.NAMESPACE);
		model.setNamespace("dcterms", DCTERMS.NAMESPACE);
		model.setNamespace("dctypes", DCTYPES_NS);
		model.setNamespace("prov", PROV_NS);
		model.setNamespace("oa", OA_NS);
		model.setNamespace("sio", SIO_NS);
		model.setNamespace("pav", PAV_NS);
		model.setNamespace("gene", GENE_NS);
		model.setNamespace("go", GO_NS);
		
		OA_ANNOTATION = valueFactory.createURI(OA_NS, "annotation");
		OA_SPECIFIC_RESOURCE = valueFactory.createURI(OA_NS, "SpecificResource");
		OA_TEXT_POSITION_SELECTOR = valueFactory.createURI(OA_NS, "TextPositionSelector");
		OA_HAS_SOURCE = valueFactory.createURI(OA_NS, "hasSource");
		OA_HAS_SELECTOR = valueFactory.createURI(OA_NS, "hasSelector");
		OA_HAS_TARGET = valueFactory.createURI(OA_NS, "hasTarget");
		OA_HAS_BODY = valueFactory.createURI(OA_NS, "hasBody");
		OA_START = valueFactory.createURI(OA_NS, "start");
		OA_END = valueFactory.createURI(OA_NS, "end");
		OA_SERIALIZED_BY = valueFactory.createURI(OA_NS, "serializedBy");
		OA_MOTIVATED_BY = valueFactory.createURI(OA_NS, "motivatedBy");
		OA_TAGGING = valueFactory.createURI(OA_NS, "tagging");
		SIO_REFERS_TO = valueFactory.createURI(SIO_NS, "refers-to");
		SUBCLASS = valueFactory.createURI(RDFS.NAMESPACE, "subClassOf");
		PUB_ANNOTATION = valueFactory.createURI("http://pubannotation.org");
		
		srCnt = 0;
		annCnt = 0;
	}
	
	public void setNamespace(String ns, String uri) {
		model.setNamespace(ns, uri);
	}

	public void addAll(RepositoryResult<Statement> statements) throws RepositoryException {
		Iterations.addAll(statements, model);
	}

	public void addEntity(String baseNamespace, String annotationPrefix, String source,
			String entityID, int spanStart, int spanEnd, URI entityDenotation) throws RepositoryException {
			
		
		// The structure of the Annotation itself
//	    <PMC-1134658-Ann1> a oa:Annotation .
//	    <PMC-1134658-Ann1> oa:serializedBy <http://pubannotation.org> .
//	    <PMC-1134658-Ann1> oa:motivatedBy oa:tagging .
		annCnt++;
		URI annotation = valueFactory.createURI(baseNamespace, annotationPrefix+"Ann"+"-"+annCnt);
		repositoryConn.add(valueFactory.createStatement(annotation, RDF.TYPE, OA_ANNOTATION));
		repositoryConn.add(valueFactory.createStatement(annotation, OA_SERIALIZED_BY, PUB_ANNOTATION));
		repositoryConn.add(valueFactory.createStatement(annotation, OA_MOTIVATED_BY, OA_TAGGING));

		// Next try to fill in the annotation-specific details
//	    <PMC-1134658-Ann1> oa:hasTarget <PMC-1134658-0-SR1> .
//	    <PMC-1134658-Ann1> oa:hasBody <PMC-1134658-0-T13-assertion> .  

		
		// Build the SpecificResource for the span of text
		
		// First, build the TextPositionSelector
		URI textSelector = valueFactory.createURI(baseNamespace, annotationPrefix+"S"+spanStart+"-"+spanEnd);
		repositoryConn.add(valueFactory.createStatement(textSelector, RDF.TYPE, OA_TEXT_POSITION_SELECTOR));
		repositoryConn.add(valueFactory.createStatement(textSelector, OA_START, valueFactory.createLiteral(spanStart)));
		repositoryConn.add(valueFactory.createStatement(textSelector, OA_END, valueFactory.createLiteral(spanEnd)));		
		
		// Then, build the SpecificResource that uses that Selector
		srCnt++;
		URI specificResource = valueFactory.createURI(baseNamespace, annotationPrefix+"SR"+srCnt);
		repositoryConn.add(valueFactory.createStatement(specificResource, RDF.TYPE, OA_SPECIFIC_RESOURCE));
		repositoryConn.add(valueFactory.createStatement(specificResource, OA_HAS_SOURCE, valueFactory.createURI(source)));
		repositoryConn.add(valueFactory.createStatement(specificResource, OA_HAS_SELECTOR, textSelector));
		// Connect to Annotation
		repositoryConn.add(valueFactory.createStatement(annotation, OA_HAS_TARGET, specificResource));

		
		/* Now need to construct named graph for content
		 PMC-1134658-0-T13-assertion {
		  <PMC-1134658-0-T13> rdfs:subClassOf genia:Protein .
		  }
		  */
		String rdfEntityID = annotationPrefix+entityID;
//		System.out.println("Named Graph for " + entityID + ", " + rdfEntityID);
		URI entityURI = valueFactory.createURI(baseNamespace, rdfEntityID);
		idMap.put(entityID, entityURI);

		URI assertionGraph = valueFactory.createURI(baseNamespace, rdfEntityID+"-assertion");
		assertionMap.put(entityID, assertionGraph);

		// Using assertionGraph as CONTEXT (Named Graph)
		repositoryConn.add(valueFactory.createStatement(entityURI, SUBCLASS, entityDenotation), assertionGraph);
		// Connect to Annotation
		repositoryConn.add(valueFactory.createStatement(annotation, OA_HAS_BODY, assertionGraph));

	}
	
	public void addRelation(String baseNamespace, String annotationPrefix, String source,
			String relationID, URI predURI, String subj, String obj) throws RepositoryException {

		// Introduce the Relation as an instance of the predicate
//		URI relationURI = valueFactory.createURI(baseNamespace, annotationPrefix+relationID);
		
		// Translate subj/obj to existing IDs (assumes Entities have already been processed)
		URI mappedSubjID = idMap.get(subj);
		URI mappedObjID = idMap.get(obj);
		
		// Use OBJECT as context. In the examples, this is the "main" relevant event
		// NB: This might be a bad assumption.
		URI objAssertionGraph = assertionMap.get(obj);
		
		if ( mappedSubjID == null || mappedObjID == null ) {
			throw new RepositoryException("No known mapping for either " + subj + " or " + obj);
		}
		
//		idMap.put(relationID, relationURI);
		// Using objAssertionGraph as CONTEXT (Named Graph)
		repositoryConn.add(valueFactory.createStatement(mappedSubjID, predURI, mappedObjID), objAssertionGraph);

		
	}
	
	public Model getModel() {
		return model;
	}
}
