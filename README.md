# README #

### What is this repository for? ###

This is a project, with code in Java using Sesame, which supports translation of the PubAnnotation JSON format (see pubannotation.org) to the Open Annotation RDF standard (see the W3C Web Annotation Working Group page, http://www.w3.org/annotation/).

### Who do I talk to? ###

* Karin Verspoor karin.verspoor@unimelb.edu.au